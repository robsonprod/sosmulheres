import {Component, ViewChild} from '@angular/core';
import {Platform, ionicBootstrap, Nav} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {Http} from 'angular2/http';
import 'rxjs/add/operator/map';

import {TabsPage} from './pages/tabs/tabs';
import {LoginPage} from './pages/login/login';
import {FriendsPage} from './pages/friends/friends';
import {ContatoPage} from './pages/contato/contato';
import {Satangos} from './util/satangos';
import {Fire} from './util/fire';
import {ConnectivityService} from './providers/connectivity-service/connectivity-service';


@Component({
  templateUrl: 'build/app.html',
  providers: [Fire, Http, Satangos, ConnectivityService]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = TabsPage;

  isLogin: any;


  pages: Array<{ title: string, component: any }>;

  constructor(private platform: Platform, private fire: Fire) {
    this.initializeApp();


    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Login', component: LoginPage },
      { title: 'Fale conosco', component: ContatoPage }
    ];
  }

  verificaLogin(){
      if(this.fire.isLogued() === false){
         this.isLogin = false;
     }else{
         this.isLogin = true;
     }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
  goToHomePage() {
    this.nav.setRoot(TabsPage);
  }
  goToFriendsPage(){
    this.nav.setRoot(FriendsPage);
  }

}

ionicBootstrap(MyApp, null, {
    tabbarPlacement: 'top'
});
