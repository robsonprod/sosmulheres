import {Component, OnInit, OnDestroy} from '@angular/core';
import {NavController, Loading, ModalController} from 'ionic-angular';
import {Http, Headers} from 'angular2/http';
import {FORM_DIRECTIVES, FormBuilder, Validators, FormGroup, AbstractControl} from '@angular/forms';
import 'rxjs/add/operator/map';

import {FacebookLogin} from '../../util/facebook-login';
import {Fire} from '../../util/fire';
import {TabsPage} from '../tabs/tabs';
import {ModalLoginPage} from '../modal-login/modal-login';

declare var firebase: any;

@Component({
  templateUrl: 'build/pages/login/login.html',
    directives: [FORM_DIRECTIVES]
})
export class LoginPage implements OnInit, OnDestroy {

  authForm: FormGroup;
  email: AbstractControl;
  password: AbstractControl;


  user: any = {};
  users: any = [];
  response: string;

  constructor(private nav: NavController, private fb: FormBuilder, private fire: Fire, public modal: ModalController) {
    this.authForm = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });

    this.email = this.authForm.controls['email'];
    this.password = this.authForm.controls['password'];
  }

  ngOnDestroy() {
    console.log('ngOnDestroy');
  }

  ngOnInit() {
    console.log('ngOnInit');
  }

  //deu certo
  onSubmit(value: string): void {
    if (this.authForm.valid) {
      console.log('Submitted value: ', value);
      this.fire.newUser(this.authForm.value.email, this.authForm.value.password);
      this.nav.setRoot(TabsPage);
    }
  }

  loginModal(){
      let modal = this.modal.create(ModalLoginPage);
      modal.present();
  }

  onLoginFacebook() {
    FacebookLogin.login(response => {
      this.fire.loginFacebook(response.accessToken, () => {
        this.nav.setRoot(TabsPage);
      }, error => {
        alert(error);
      });
    }, error => {
      alert(error.errorMessage);
    });
  }

}
