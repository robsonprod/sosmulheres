import {Component, NgZone} from '@angular/core';
import {Geolocation} from 'ionic-native';
import {NavController, ModalController, AlertController, LoadingController, Platform, NavParams} from 'ionic-angular';
import {Http} from '@angular/http';
import {Response, Headers, RequestOptions} from 'angular2/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import {Network} from 'ionic-native';

import {ModalViolacaoMarkPage} from '../modal-violacao-mark/modal-violacao-mark';
import {ModalViolacaoInfoPage} from '../modal-violacao-info/modal-violacao-info';
import {Satangos} from '../../util/satangos';
import {ConnectivityService} from '../../providers/connectivity-service/connectivity-service';

declare var google

@Component({
  templateUrl: 'build/pages/maps/maps.html',
  directives: [ModalViolacaoMarkPage, ModalViolacaoInfoPage]
})

export class MapsPage {
  map: any;
  cacheMarkers: any;
  tipoCrime: any;
  position: any;
  onDevice: boolean;
  markers: any;

  mapInitialised: boolean = false;
  apiKey: any = "AIzaSyArB0bB25GhspmnS1NyKcLbKT2o4jiOQfI";

  constructor(private nav: NavController,
    private http: Http,
    private modal: ModalController,
    private alert: AlertController,
    private loading: LoadingController,
    private _ngZone: NgZone,
    private params: NavParams,
    private satan: Satangos,
    private connectService: ConnectivityService) {

    this.markers = [];
    this.loadGoogleMaps();

  }



  loadGoogleMaps() {
    this.addConnectivityListeners();

    if (typeof google == "undefined" || typeof google.maps == "undefined") {

      if (this.connectService.isOnline()) {

        //Load the SDK
        (<any>window).mapInit = () => {
          console.log("initPage()")
          this.initPage();
          this.enableMap();
        }

        let script = document.createElement("script");
        script.id = "googleMaps";

        if (this.apiKey) {
          script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
        } else {
          script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
        }

        document.body.appendChild(script);
      }
    } else {
      console.log("não deu pra carregar");

      if (this.connectService.isOnline()) {
        console.log("abrindo map");
        this.initPage();
      }
      else {
        alert("celular não esta conectado");
        this.disableMap();
      }

    }

  }
  disableMap() {
    console.log("disable map");
  }

  enableMap() {
    console.log("enable map");
  }

  private getAddress(latLng, successCallback) {
    let geocoder = new google.maps.Geocoder;

    geocoder.geocode({ location: latLng }, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          successCallback(results[0].formatted_address);
        }
      }
    });
  }

  private initPage() {
    let options = { timeout: 1000, enableHighAccuracy: true };
    Geolocation.getCurrentPosition(options).then(position => {
        this.initMap(position.coords.latitude, position.coords.longitude);
    }, (err) => {
      console.log("Erro initPage = ", err);
      this.initMap(-2.910822, -41.744546);
    });

    let subscription = Geolocation.watchPosition().subscribe(position => {
      console.log(position.coords.longitude + ' ' + position.coords.latitude);
    });
  }

  private initMap(lat, lng) {

    this.mapInitialised = true;

    let latLng = new google.maps.LatLng(lat, lng);

    let mapOptions = {
      disableDefaultUI: true,
      setMyLocationEnabled: true,
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    let element = document.getElementById("map");
    this.map = new google.maps.Map(element, mapOptions);

    google.maps.event.addListener(this.map, 'click', (LatLng) => {
      this._ngZone.run(() => {
        this.modalViolacaoMarker(LatLng);
      });
    });

    // chamando metodo de getendereço
    // this.getAddress(latLng, address => {
    //   this.position.lat = latLng.lat();
    //   this.position.lng = latLng.lng();
    //   this.position.address = address;
    //   console.log(this.position);
    // });
  }


  addConnectivityListeners() {
    let me = this;

    let onOnline = () => {
      setTimeout(() => {
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
          this.loadGoogleMaps();
        } else {
          if (!this.mapInitialised) {
            this.initPage();
          }
        }
      }, 2000);
    };

    let onOffline = () => {
      console.log("celular esta offline");
      alert("celular esta sem internet");
    };

    document.addEventListener('online', onOnline, false);
    document.addEventListener('offline', onOffline, false);

  }


  modalViolacaoMarker(LatLng) {
    let modal = this.modal.create(ModalViolacaoMarkPage, { 'LatLng': LatLng, 'satan': this.satan });

    modal.onDidDismiss(data => {
      if (data !== undefined) {
        console.log(data);
        let marker = new google.maps.Marker({
          position: new google.maps.LatLng(data.latitude, data.longitude),
          map: this.map,
          animation: google.maps.Animation.DROP,
        });

        marker.setMap(this.map);
        this.addInfoWindow(marker, data);
        this.getCrimes();
      }
    });

    modal.present();
  }

  addInfoWindow(marker, crime) {

    google.maps.event.addListener(marker, 'click', () => {
      this._ngZone.run(() => {
        this.modalViolacaoInfo(crime);
      });
    });

  }

  modalViolacaoInfo(dados) {
    let modal = this.modal.create(ModalViolacaoInfoPage, { 'dados': dados });

    // modal.onDidDismiss(data => {
    //   console.log("modal informativo", data);
    // });

    modal.present();
  }

  getCrimes() {
    this.satan.getRequest('crimes').subscribe(
      data => {
        this.setMarkers(data);
      }
    );
  }

  setMarkers(crimes) {
    if (crimes != null) {
      crimes.forEach(crime => {
        let marker = new google.maps.Marker({
          position: new google.maps.LatLng(crime.latitude, crime.longitude),
          map: this.map,
          animation: google.maps.Animation.DROP,
        });

        marker.setMap(this.map);
        this.addInfoWindow(marker, crime);
        this.markers.push(marker);
      });
    }

  }

  setFiltro(tipoCrime) {
    this.clearMarks();
    this.satan.getPorTipo('busca', tipoCrime).subscribe(
      data => {
        this.setMarkers(data);
      }
    );
  }

  clearMarks() {
    // this.markers = [];

    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    // this.setMarkers(null);
  }

}
