import { Component } from '@angular/core';
import { NavController, ViewController} from 'ionic-angular';
import {FORM_DIRECTIVES, FormBuilder, Validators, FormGroup, AbstractControl} from '@angular/forms';

import {HomePage} from '../home/home';
import {LoginPage} from '../login/login';


@Component({
  templateUrl: 'build/pages/modal-login/modal-login.html',
  directives: [FORM_DIRECTIVES]
})
export class ModalLoginPage {
    firebase: any;

    loginForm: FormGroup;
    email: AbstractControl;
    password: AbstractControl;

  constructor(private nav: NavController, private view: ViewController, private fb: FormBuilder) {
      this.loginForm = fb.group({
        'email': ['', Validators.compose([Validators.required])],
        'password': ['', Validators.compose([Validators.required])]
      });
  }

  cancel(){
      this.view.dismiss();
  }

  onLogin(value: string): void {
    //   acho que resolvido com getDB()
    console.log("problema com firebase sendo inicializado duas vezes");
    this.nav.setRoot(HomePage);
  }

}
