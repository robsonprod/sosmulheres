import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

/*
  Generated class for the ModalViolacaoInfoPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/modal-violacao-info/modal-violacao-info.html',
})
export class ModalViolacaoInfoPage {

  dado: any;

  constructor(private navCtrl: NavController, private view: ViewController, private params: NavParams) {
    console.log("Data modal info: ", params.get('dados'));

    this.dado = params.get('dados');
  }

  cancel() {
    this.view.dismiss();
  }

}
