import { Component } from '@angular/core';
import { NavController, ViewController, AlertController, NavParams} from 'ionic-angular';
// import { FormBuilder, Validators, ControlGroup, AbstractControl} from '@angular/common';
import {FORM_DIRECTIVES, FormBuilder, Validators, FormGroup, AbstractControl} from '@angular/forms';

import {TabsPage} from '../tabs/tabs';
import {MapsPage} from '../maps/maps';


@Component({
  templateUrl: 'build/pages/modal-violacao-mark/modal-violacao-mark.html',
  directives: [FORM_DIRECTIVES],
})
export class ModalViolacaoMarkPage {

  formViolacao: FormGroup;
  private satan;

  constructor(private navCtrl: NavController,
    private view: ViewController,
    public fb: FormBuilder,
    private alert: AlertController,
    private params: NavParams) {

    let coords = this.params.get('LatLng');
    this.satan = this.params.get('satan');

    this.formViolacao = this.fb.group({
      "data": [""],
      "hora": [""],
      "tipo": [""],
      "latitude": [coords.latLng.lat()],
      "longitude": [coords.latLng.lng()]
    });
  }

  cancel() {
    this.view.dismiss();
  }

  onSubmit(value: string): void {
    let confirm = this.alert.create({
      title: 'Comfirmação',
      message: 'Você tem certeza?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('cancel clickado');
          this.cancel();
        }
      }, {
          text: 'Sim',
          handler: () => {
            if (this.formViolacao.valid) {
              this.view.dismiss(value);
              this.satan.addRequest('create', value);
            }
          }
        }]
    });

    confirm.present();
  }

  // showConfirmation(value: string){
  //     let confirm = this.alert.create({
  //        title: 'Comfirmação',
  //        message: 'Você tem certeza?',
  //        buttons: [{
  //            text: 'Cancel',
  //            handler: () => {
  //                console.log('cancel clickado');
  //            }
  //        },{
  //            text: 'Sim',
  //            handler: () => {
  //                console.log('sim clickado');
  //            }
  //        }]
  //     });
  //     confirm.present();
  // }

}
