import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {HomePage} from '../home/home';
import {MapsPage} from '../maps/maps';

/*
  Generated class for the TabsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/tabs/tabs.html',
})
export class TabsPage {

    private tab1Root: any;
    private tab2Root: any;

  constructor(private nav: NavController) {
      this.tab1Root = HomePage;
      this.tab2Root = MapsPage;
  }

}
