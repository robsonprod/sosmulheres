import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';
import {Network} from 'ionic-native';
import 'rxjs/add/operator/map';

@Injectable()
export class ConnectivityService {

  onDevice: boolean;

  constructor(private platform: Platform){
    this.onDevice = this.platform.is('ios') || this.platform.is('android');
  }

  isOnline() {
    if(this.onDevice && Network.connection){

      let networkState = Network.connection;

      return networkState;

    } else {
      return navigator.onLine;
    }
  }

  isOffline(){
    if(this.onDevice && Network.connection){

      let networkState = Network.connection;

      return networkState;

    } else {
      return !navigator.onLine;
    }
  }
}
