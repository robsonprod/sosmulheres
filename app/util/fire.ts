import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

declare var firebase: any;

@Injectable()
export class Fire {

  user: any = {};
  http: any;

  isLogin = false;

  constructor(http: Http) {
    var config = {
      apiKey: "AIzaSyDURQ74P8HKt1wgROpq-LxArw1wz7o6PfE",
      authDomain: "descarteecologico-1380.firebaseapp.com",
      databaseURL: "https://descarteecologico-1380.firebaseio.com",
      storageBucket: "descarteecologico-1380.appspot.com",
    };

    firebase.initializeApp(config);

  }

  getDB() {
      return firebase;
  }

  loginFacebook(token: string, successCallback, errorCallback) {
    let credential = firebase.auth.FacebookAuthProvider.credential(token);

    firebase.auth().signInWithCredential(credential).then(response => {
      this.setUser(token, response.providerData[0]);
      successCallback();
    }, error => {
      errorCallback(error);
    })
  }


  private setUser(token: string, authData: any) {
    this.user.name = authData.displayName;
    this.user.photo = authData.photoURL;
    this.user.id = authData.uid;
    this.user.token = token;

    this.saveUser();
  }


  private saveUser() {
    firebase.database().ref('users').child(this.user.id).set({
      name: this.user.name,
      photo: this.user.photo
    }, error => {
      console.log(error);
    });
  }

  // teste
  newUser(email: string, password: string) {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((newUser) => {
        console.log('logou (newUser)');
      }, (error) => {
        console.log(error + 'erro no newUser');
      });
  }

  // modal
  loginUser(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password)
      .then((loginUser) => {
        console.log('loginUser successfully');
      }, (error) => {
        console.log('loginUser errorCallback' + error);
      });
  }
  // verifica se esta logado
  isLogued() {
    let result;
    firebase.auth().onAuthStateChanged((_currentUser) => {
      if (_currentUser) {
        console.log("User " + _currentUser.uid + " is logged in with " + _currentUser.provider);
        let user = firebase.auth().currentUser;
        result = true;
      } else {
        console.log("fire.ts(isLogued) = User is logged out");
        result = false;
      }
    });
    return result;
  }
  // desloga o user
  logout() {
    firebase.auth().signOut();
  }

}
