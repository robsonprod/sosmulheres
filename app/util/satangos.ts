import {Injectable} from '@angular/core';
import {Headers, RequestOptions} from 'angular2/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';


@Injectable()
export class Satangos {
  private API_URL: string = "http://192.168.1.105/sosmulheres/api/v1/";
  // private API_URL: string = "https://forum.ionicframework.com/";
  public responseCallback: Function;

  constructor(private http: Http) {
  }

  getRequest(uri) {
    return this.http.get(this.API_URL + uri).map((res: Response) => res.json());
  }

  addRequest(uri, data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post(this.API_URL + uri, data)
      .subscribe(res => {
       	console.log(res.json());
      }, (err) => {
       	console.log(err);
      });
  }

  getPorTipo(uri, tipoCrime){
      return this.http.get(this.API_URL + uri + "/" + tipoCrime).map((res: Response) => res.json());
  }

}
